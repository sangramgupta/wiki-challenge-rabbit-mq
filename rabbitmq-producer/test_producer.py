import unittest
from unittest.mock import Mock, patch
import producer
import pika

class TestYourCode(unittest.TestCase):

    @patch('producer.pika.BlockingConnection')
    def test_connect_to_rabbitmq(self, mock_blocking_connection):
        # Mock the BlockingConnection class
        mock_connection = Mock()
        mock_blocking_connection.return_value = mock_connection

        # Call the connect_to_rabbitmq function
        channel = producer.connect_to_rabbitmq()

        # Assert that BlockingConnection is called with the correct parameters
        mock_blocking_connection.assert_called_once_with(
            pika.ConnectionParameters(
                host=producer.RABBITMQ_HOST,
                credentials=producer.RABBITMQ_CREDENTIALS,
                virtual_host=producer.RABBITMQ_VIRTUAL_HOST
            )
        )

        # Assert that the returned channel is the same as the mock connection's channel
        self.assertEqual(channel, mock_connection.channel.return_value)

if __name__ == "__main__":
    unittest.main()
