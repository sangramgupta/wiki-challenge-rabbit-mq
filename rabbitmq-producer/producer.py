import csv
import time
import random
import pika
import json

# RabbitMQ connection parameters
RABBITMQ_CREDENTIALS = pika.PlainCredentials('virtualuser', '123456')
RABBITMQ_HOST = 'localhost'
RABBITMQ_VIRTUAL_HOST = "wikiSpace"
QUEUE_NAME = 'demoqueue'

# Define the path to the sample data file
DATA_FILE_PATH = '../data/de_challenge_sample_data.csv'


def random_interval() -> float:
    """
    Generate a random interval between 0 and 1 second.

    Returns:
        float: Random interval.
    """
    return random.uniform(0, 1)


def connect_to_rabbitmq() -> pika.channel.Channel:
    """
    Establish a connection to RabbitMQ and return the channel.

    Returns:
        pika.channel.Channel: RabbitMQ channel.
    """
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=RABBITMQ_HOST,
        credentials=RABBITMQ_CREDENTIALS,
        virtual_host=RABBITMQ_VIRTUAL_HOST
    ))
    return connection.channel()


def publish_edit(channel: pika.channel.Channel, edit: dict) -> None:
    """
    Publish a message to the RabbitMQ queue.

    Args:
        channel (pika.channel.Channel): RabbitMQ channel.
        edit (dict): Edit data as a dictionary.
    """
    channel.basic_publish(
        exchange='wiki',
        routing_key='edits',
        body=json.dumps(edit),
        properties=pika.BasicProperties(
            delivery_mode=2,  # Make the message persistent
        )
    )
    print(f"Sent: {edit}")


def main() -> None:
    # Connect to RabbitMQ
    channel = connect_to_rabbitmq()

    # Declare the queue (create it if it doesn't exist)
    channel.exchange_declare(exchange='wiki', exchange_type='topic', durable=True)

    # Read the sample data from the CSV file
    with open(DATA_FILE_PATH, 'r', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)

        for row in reader:
            edit = {
                "$schema": row["$schema"],
                "id": row["id"],
                "type": row["type"],
                "namespace": row["namespace"],
                "title": row["title"],
                "comment": row["comment"],
                "timestamp": int(row["timestamp"]),
                "user": row["user"],
                "bot": row["bot"],
                "minor": row["minor"],
                "patrolled": row["patrolled"],
                "server_url": row["server_url"],
                "server_name": row["server_name"],
                "server_script_path": row["server_script_path"],
                "wiki": row["wiki"],
                "parsedcomment": row["parsedcomment"],
                "meta_domain": row["meta_domain"],
                "meta_uri": row["meta_uri"],
                "meta_request_id": row["meta_request_id"],
                "meta_stream": row["meta_stream"],
                "meta_topic": row["meta_topic"],
                "meta_dt": row["meta_dt"],
                "meta_partition": row["meta_partition"],
                "meta_offset": row["meta_offset"],
                "meta_id": row["meta_id"],
                "length_old": row["length_old"],
                "length_new": row["length_new"],
                "revision_old": row["revision_old"],
                "revision_new": row["revision_new"]
            }

            publish_edit(channel, edit)

            # Sleep for a random interval between 0 and 1 second
            sleep_duration = random_interval()
            time.sleep(sleep_duration)

    # Close the RabbitMQ connection
    channel.close()


if __name__ == "__main__":
    main()
