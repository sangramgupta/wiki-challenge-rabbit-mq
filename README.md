# Data Transformation & Analysis Challenge 

## Project Structure
* **data/**  This folder contains sample data and questions for the challenge.
* **rabbitmq-producer/** Publisher scripts and test file, contains 2 files
  + **producer.py** Run the publisher to push data
  + **test_producer.py** Unit tests covered for functional integrity.
* **rabbitmq-consumer/** Subsciber scripts to pull data
  + **subscriber_stream_based.py** Approach 1: Runs the subscribers and considers the data coming to be streaming in real time. Calculates both Global Edits and German Edits,
  + **test_subscriber_stream_based.py** Unit tests covered for functional integrity.
  + **subscriber_windowed_timestamp.py** Approach 2: Runs the subscribers and considers timestamp from the sample data as there was ambuiguity to use it or not, then uses a custom sliding window buffering system to calculate global edits. TBD German Edits.
  + **test_subscriber_windowed_timsestamp.py** Unit tests covered for functional integrity. 
* **results/** 
  + **edits_stream_based.csv** Approach 1 results
  + **edits_sliding_buffered_window.csv** Approach 2 results
* **docker-compose.yaml** Contains the username and passwords and virtual_host name with ports expose.
* **rabbitmq-init.sh** Is run via docker-compose.yaml and contains permissions grants.

## Project Settup

Run the server with RabbitMQ configurations and server with the following command.
```shell
sudo docker-compose up
```

If you want to use GUI then do run http://localhost:15672/ and use Username: ```virtualuser``` and Password: ```123456``` as the login credentials, they are configurable via docker-compose.yaml

>Notice: If virtual_host is changed in docker-compose.yaml then please also change the scripts, my suggestion is not to change it.

Now we need to setup a python environment to run our scripts inside of. Please follow the commands
```shell
python3.8 -m venv env-rabbitmq
source env-rabbitmq/bin/activate

pip install pika==1.3.2
```

Once you enter within the python environment and the installations have been done. Simply start

Producer:
```
cd rabbitmq-producer/
python producer.py
```

Consumer Approach 1:
```
cd rabbitmq-consumer/
python subscriber_stream_based.py
```

Consumer Approach 2:
```
cd rabbitmq-consumer/
python subscriber_windowed_timestamp.py
```

Both the appraoches can run in parallel as the Topic Exchange takes care of two independent queues.  

<br />

## Architecture Questions


### 1. What would be a possible database to store the data? (Advantages/Disadvantages)
<br />

A combination for both a **Relational Database for long-term analytics** and **Apache Kafka** would be good architecture. 

* Pros: If you need to store Wikipedia change events and perform complex analytics or reporting on the data, a relational database could be a good choice.
* Cons: We will need to manage a datalake.

From my perspective it is a very open ended problem and a lot more nuance and context is needed. We can even use **Time-Series database** if our scope is only limited to number of edits per minute. 

* Pros: If you want to analyze Wikipedia change events based on timestamps and perform time-based aggregations (e.g., edits per minute), a time-series database could be beneficial.
* Cons: Limited capabilities and use case.

I had also come up with **Apache Hbase with Apache Spark**, but it is a complex architecture which can be used but the orchestration and management overhead is significant.

* Pros: Fast, complex ETL can be handled easily, scalable solution and has Spark streaming service for real time analytics.
* Cons: Operational overhead, resource hungry architecture.  
<br />

### 2. Which data model do you think would be useful for storing the data events?
<br />

Network data model will be suitable in ths case.  
<br />

### 3. Which exchange model would make sense? Describe advantages/disadvantages of the of Exchanges you choose in terms of scalability and fault tolerance.
<br />

In RabbitMQ Topic Exchange is what I think makes sense. It provides flexible routing based on wildcard patterns in the routing key.
Also, topic exchange is suitable for complex routing scenarios where messages should be delivered to specific consumers based on criteria.

Some disadvantages can be as the system grows, it will require more configuration and understanding of routing patterns. May result in performance overhead for complex routing logic.
