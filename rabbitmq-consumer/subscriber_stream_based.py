import pika
import datetime
import json

# Define RabbitMQ connection parameters
RABBITMQ_USERNAME = 'virtualuser'
RABBITMQ_PASSWORD = '123456'
RABBITMQ_HOST = 'localhost'
RABBITMQ_VIRTUAL_HOST = 'wikiSpace'
QUEUE_NAME = 'qu1'
RESULTS_CSV = '../results/edits_stream_based.csv'

# Initialize counters and timer
global_edits_per_minute = 0
german_wikipedia_edits_per_minute = 0
timer_started = False
base_time_counter = None

# Callback function to handle incoming messages
def process_message(ch, method, properties, body):
    """
    Process an incoming message from RabbitMQ.

    Args:
        ch: The channel.
        method: Delivery properties.
        properties: Message properties.
        body: The message body.
    """
    global global_edits_per_minute
    global german_wikipedia_edits_per_minute
    global timer_started
    global base_time_counter

    if not timer_started:
        timer_started = True
        base_time_counter = datetime.datetime.now()

    # Calculate the current time
    current_time = datetime.datetime.now()

    # Parse the message and check if it's from the German Wikipedia
    message = body.decode()
    row = json.loads(message)
    wiki = row["wiki"]

    # Increment the global edits counter
    global_edits_per_minute += 1

    if wiki == 'dewiki':
        german_wikipedia_edits_per_minute += 1

    # Check if a new minute has started
    minute_check = (current_time - base_time_counter).total_seconds()
    if minute_check < 60:
        # Save the aggregations to a file or a database
        print_aggregations(current_time, global_edits_per_minute, german_wikipedia_edits_per_minute)
    else:
        save_aggregations(current_time, global_edits_per_minute, german_wikipedia_edits_per_minute)
        # Reset counters for the new minute
        base_time_counter = datetime.datetime.now()
        global_edits_per_minute = 1
        german_wikipedia_edits_per_minute = 1 if wiki == 'dewiki' else 0

    print(f"Received message: {message}")

def print_aggregations(timestamp, global_edits, german_wikipedia_edits):
    """
    Print aggregation data to a file or database on stream row basis.

    Args:
        timestamp: The timestamp.
        global_edits: Global edits per minute.
        german_wikipedia_edits: German Wikipedia edits per minute.
    """
    print(f"Timestamp: {timestamp}, Global Edits/Min: {global_edits}, German Wikipedia Edits/Min: {german_wikipedia_edits}")

# Function to save aggregations to a file or database.
def save_aggregations(timestamp, global_edits, german_wikipedia_edits):
    """
    Save aggregation data to a file or database.

    Args:
        timestamp: The timestamp.
        global_edits: Global edits per minute.
        german_wikipedia_edits: German Wikipedia edits per minute.
    """
    with open(RESULTS_CSV, mode='a', newline='') as csv_file:
        # Write the new data row by row
        csv_file.write(f"Timestamp: {timestamp}, Global Edits/Min: {global_edits}, German Wikipedia Edits/Min: {german_wikipedia_edits}" + "\n")


def main():
    # Connect to RabbitMQ
    credentials = pika.PlainCredentials(RABBITMQ_USERNAME, RABBITMQ_PASSWORD)
    connection_params = pika.ConnectionParameters(
        host=RABBITMQ_HOST, credentials=credentials, virtual_host=RABBITMQ_VIRTUAL_HOST
    )
    connection = pika.BlockingConnection(connection_params)
    channel = connection.channel()

    # Declare the queue (create it if it doesn't exist) as durable
    channel.exchange_declare(exchange='wiki', exchange_type='topic', durable=True)
    result = channel.queue_declare(QUEUE_NAME, exclusive=True)
    queue_name = result.method.queue
    binding_keys = ['edits']  # Subscribe to messages with 'edits' routing key

    for binding_key in binding_keys:
        channel.queue_bind(exchange='wiki', queue=queue_name, routing_key=binding_key)

    # Set up the consumer to receive messages from the queue
    channel.basic_consume(queue=QUEUE_NAME, on_message_callback=process_message, auto_ack=True)

    print('Waiting for messages. To exit, press Ctrl+C')

    # Start consuming messages
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        # Gracefully exit on Ctrl+C
        print('Consumer stopped.')

    # Close the RabbitMQ connection
    connection.close()

if __name__ == "__main__":
    main()
