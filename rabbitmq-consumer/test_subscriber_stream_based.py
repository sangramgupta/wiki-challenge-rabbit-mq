import unittest
from unittest.mock import patch, Mock
import subscriber_stream_based

class TestProcessMessage(unittest.TestCase):

    @patch('subscriber_stream_based.datetime', Mock(wraps=subscriber_stream_based.datetime))
    def test_process_message_german_wikipedia(self):
        # Mock the datetime module to control the current time
        mock_now = Mock()
        mock_now.total_seconds.return_value = 30.0  # Less than 60 seconds

        # Initialize global variables
        subscriber_stream_based.global_edits_per_minute = 0
        subscriber_stream_based.german_wikipedia_edits_per_minute = 0
        subscriber_stream_based.timer_started = False
        subscriber_stream_based.base_time_counter = None

        # Define a test message from the German Wikipedia
        test_message = b'{"$schema": "/mediawiki/recentchange/1.0.0", "id": "1389063094", "type": "edit", "namespace": "0", "title": "Q37914681", "comment": "/* wbsetdescription-add:1|bn */ \\u09b0\\u09be\\u09b6\\u09bf\\u09af\\u09bc\\u09be\\u09b0 \\u098f\\u0995\\u099f\\u09bf \\u09a6\\u09cd\\u09ac\\u09c0\\u09aa, [[:toollabs:quickstatements/#/batch/48173|batch #48173]]", "timestamp": 1611249080, "user": "Tahmid02016", "bot": "False", "minor": "False", "patrolled": "True", "server_url": "https://www.wikidata.org", "server_name": "www.wikidata.org", "server_script_path": "/w", "wiki": "dewiki", "parsedcomment": "\\u200e<span dir=\\"auto\\"><span class=\\"autocomment\\">[bn] \\u09ac\\u09bf\\u09ac\\u09b0\\u09a3 \\u09af\\u09cb\\u0997 \\u09b9\\u09af\\u09bc\\u09c7\\u099b\\u09c7: </span></span> \\u09b0\\u09be\\u09b6\\u09bf\\u09af\\u09bc\\u09be\\u09b0 \\u098f\\u0995\\u099f\\u09bf \\u09a6\\u09cd\\u09ac\\u09c0\\u09aa, <a href=\\"https://iw.toolforge.org/quickstatements/#.2Fbatch.2F48173\\" class=\\"extiw\\" title=\\"toollabs:quickstatements/\\">batch #48173</a>", "meta_domain": "www.wikidata.org", "meta_uri": "https://www.wikidata.org/wiki/Q37914681", "meta_request_id": "YAm1uApAMNkAAuD9djEAAACH", "meta_stream": "mediawiki.recentchange", "meta_topic": "eqiad.mediawiki.recentchange", "meta_dt": "2021-01-21T17:11:20Z", "meta_partition": "0", "meta_offset": "2887301737", "meta_id": "10a1a1da-da51-4991-b248-4f3365d5aaf2", "length_old": "6582", "length_new": "6688", "revision_old": "898245132", "revision_new": "1345601865"}'

        # Call the process_message function with the test message
        subscriber_stream_based.process_message(None, None, None, test_message)

        # Assert that the global counters have been updated correctly
        self.assertTrue(subscriber_stream_based.timer_started)
        self.assertIsNotNone(subscriber_stream_based.base_time_counter)
        self.assertEqual(subscriber_stream_based.global_edits_per_minute, 1)
        self.assertEqual(subscriber_stream_based.german_wikipedia_edits_per_minute, 1)

    @patch('subscriber_stream_based.datetime', Mock(wraps=subscriber_stream_based.datetime))
    def test_process_message_other_wikipedia(self):
        # Mock the datetime module to control the current time
        mock_now = Mock()
        mock_now.total_seconds.return_value = 30.0  # Less than 60 seconds

        # Initialize global variables
        subscriber_stream_based.global_edits_per_minute = 0
        subscriber_stream_based.german_wikipedia_edits_per_minute = 0
        subscriber_stream_based.timer_started = False
        subscriber_stream_based.base_time_counter = None

        # Define a test message from a non-German Wikipedia
        test_message = b'{"$schema": "/mediawiki/recentchange/1.0.0", "id": "1389063094", "type": "edit", "namespace": "0", "title": "Q37914681", "comment": "/* wbsetdescription-add:1|bn */ \\u09b0\\u09be\\u09b6\\u09bf\\u09af\\u09bc\\u09be\\u09b0 \\u098f\\u0995\\u099f\\u09bf \\u09a6\\u09cd\\u09ac\\u09c0\\u09aa, [[:toollabs:quickstatements/#/batch/48173|batch #48173]]", "timestamp": 1611249080, "user": "Tahmid02016", "bot": "False", "minor": "False", "patrolled": "True", "server_url": "https://www.wikidata.org", "server_name": "www.wikidata.org", "server_script_path": "/w", "wiki": "wikidatawiki", "parsedcomment": "\\u200e<span dir=\\"auto\\"><span class=\\"autocomment\\">[bn] \\u09ac\\u09bf\\u09ac\\u09b0\\u09a3 \\u09af\\u09cb\\u0997 \\u09b9\\u09af\\u09bc\\u09c7\\u099b\\u09c7: </span></span> \\u09b0\\u09be\\u09b6\\u09bf\\u09af\\u09bc\\u09be\\u09b0 \\u098f\\u0995\\u099f\\u09bf \\u09a6\\u09cd\\u09ac\\u09c0\\u09aa, <a href=\\"https://iw.toolforge.org/quickstatements/#.2Fbatch.2F48173\\" class=\\"extiw\\" title=\\"toollabs:quickstatements/\\">batch #48173</a>", "meta_domain": "www.wikidata.org", "meta_uri": "https://www.wikidata.org/wiki/Q37914681", "meta_request_id": "YAm1uApAMNkAAuD9djEAAACH", "meta_stream": "mediawiki.recentchange", "meta_topic": "eqiad.mediawiki.recentchange", "meta_dt": "2021-01-21T17:11:20Z", "meta_partition": "0", "meta_offset": "2887301737", "meta_id": "10a1a1da-da51-4991-b248-4f3365d5aaf2", "length_old": "6582", "length_new": "6688", "revision_old": "898245132", "revision_new": "1345601865"}'

        # Call the process_message function with the test message
        subscriber_stream_based.process_message(None, None, None, test_message)

        # Assert that the global counters have been updated correctly
        self.assertTrue(subscriber_stream_based.timer_started)
        self.assertIsNotNone(subscriber_stream_based.base_time_counter)
        self.assertEqual(subscriber_stream_based.global_edits_per_minute, 1)
        self.assertEqual(subscriber_stream_based.german_wikipedia_edits_per_minute, 0)

if __name__ == '__main__':
    unittest.main()