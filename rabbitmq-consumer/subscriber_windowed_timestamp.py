import pika
import json

# Define RabbitMQ connection parameters
RABBITMQ_USERNAME = 'virtualuser'
RABBITMQ_PASSWORD = '123456'
RABBITMQ_HOST = 'localhost'
RABBITMQ_VIRTUAL_HOST = 'wikiSpace'
QUEUE_NAME = 'qu2'
ROWS = 2
COLUMNS = 60
RESULTS_CSV = '../results/edits_sliding_buffered_window.csv'

# Initialize counters and timer
buffer_size = ROWS * COLUMNS
buffer = [0] * buffer_size


def initialize_buffer() -> None:
    """
    Initializes the buffer with zeros.
    """
    for i in range(buffer_size):
        buffer[i] = 0


# Callback function to handle incoming messages
def process_message(ch, method, properties, body) -> None:
    """
    Process an incoming message from RabbitMQ.

    Args:
        ch: The channel.
        method: Delivery properties.
        properties: Message properties.
        body: The message body.
    """
    global buffer

    # Parse the message and check if it's from the German Wikipedia
    message = body.decode()
    row = json.loads(message)
    timestamp = row["timestamp"]
    exists = False

    for column in range(COLUMNS):
        if timestamp == buffer[0 * COLUMNS + column]:
            buffer[1 * COLUMNS + column] += 1
            exists = True

    if not exists:
        remainder = timestamp % 60
        buffer[0 * COLUMNS + remainder] = timestamp
        buffer[1 * COLUMNS + remainder] = 1
        for num in range(remainder):
            if timestamp > buffer[0 * COLUMNS + remainder] + 60:
                buffer[0 * COLUMNS + num] = 0
                buffer[1 * COLUMNS + num] = 0

    global_edit_window = sum(buffer[1 * COLUMNS + column] for column in range(COLUMNS))

    print(f"Received message: {message}" + "\n")
    print(f"Timestamp: {timestamp}, Global Edits/Min: {global_edit_window}" + "\n")
    save_aggregations(timestamp, global_edit_window)

# Save Global Edits
def save_aggregations(timestamp, global_edit_window):
    """
    Save aggregation data to a file or database.

    Args:
        timestamp: The timestamp.
        global_edits: Global edits per minute.
        german_wikipedia_edits: German Wikipedia edits per minute.
    """
    with open(RESULTS_CSV, mode='a', newline='') as csv_file:
        # Write the new data row by row
        csv_file.write(f"Timestamp: {timestamp}, Global Edits/Min: {global_edit_window}" + "\n")


def main() -> None:
    # Connect to RabbitMQ
    credentials = pika.PlainCredentials(RABBITMQ_USERNAME, RABBITMQ_PASSWORD)
    connection_params = pika.ConnectionParameters(
        host=RABBITMQ_HOST, credentials=credentials, virtual_host=RABBITMQ_VIRTUAL_HOST
    )
    connection = pika.BlockingConnection(connection_params)
    channel = connection.channel()

    # Declare the queue (create it if it doesn't exist) as durable
    channel.exchange_declare(exchange='wiki', exchange_type='topic', durable=True)
    result = channel.queue_declare(QUEUE_NAME, exclusive=True)
    queue_name = result.method.queue
    binding_keys = ['edits']  # Subscribe to messages with 'edits' routing key

    for binding_key in binding_keys:
        channel.queue_bind(exchange='wiki', queue=queue_name, routing_key=binding_key)

    # Set up the consumer to receive messages from the queue
    channel.basic_consume(queue=QUEUE_NAME, on_message_callback=process_message, auto_ack=True)

    print('Waiting for messages. To exit, press Ctrl+C')

    # Start consuming messages
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        # Gracefully exit on Ctrl+C
        print('Consumer stopped.')

    # Close the RabbitMQ connection
    connection.close()


if __name__ == "__main__":
    initialize_buffer()
    main()
