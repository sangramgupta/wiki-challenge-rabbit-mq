# Data Transformation & Analysis Challenge 
### (Translated from German to English)

## Context:
Our customer, a media company, is interested in current trends on Wikipedia and historical changes in topics. For this purpose, all changes made to Wikipedia should be saved, processed and the results displayed in a dashboard. A system is planned that provides the change events using RabbitMQ and writes them into a suitable database in order to carry out analyzes there with a suitable query engine.

## Task:
A RabbtiqMQ instance should be set up as a prototype to test and demonstrate possible scenarios. The following components should be programmed as part of the prototype:
* A producer that reads the sample data and at random intervals
emitted between 0-1 second. Use an exchange that makes sense for the task.
* In RabbitMQ Consumer, which reads this data from a queue, performs the foollowing aggregations and saves the results:
  * Global number of edits per minute
  * Number of edits on the German Wikipedia per minute

The components can be implemented in a language of your choice.
The submission should include the following artifacts:
 * The executable prototype in a Git repository
 * A suitable docker-compose.yml file
 * A readme file with the necessary setup and execution steps
 * The code should implement meaningful test coverage

In addition to the executable code, the following aspects are interesting to us:
1. What would be a possible database to store the data? (Before-/
Disadvantages)
2. Which data model do you think would be useful for storing the data?
Events?
3. Which exchange model would make sense? Describe advantages/disadvantages of the of Exchanges you choose in terms of scalability and fault tolerance.
