#!/bin/sh

check_rabbitmq() {
  curl -s -u $RABBITMQ_DEFAULT_USER:$RABBITMQ_DEFAULT_PASS http://localhost:15672/api/healthchecks/node -o /dev/null
}

# Wait for RabbitMQ to start
while ! check_rabbitmq; do
  echo "Waiting for RabbitMQ to start..."
  sleep 5
done

# Create the custom virtual host
rabbitmqctl add_vhost $RABBITMQ_DEFAULT_VHOST

# Create the custom user with privileges on the custom virtual host
rabbitmqctl add_user $RABBITMQ_DEFAULT_USER $RABBITMQ_DEFAULT_PASS
rabbitmqctl set_permissions -p $RABBITMQ_DEFAULT_VHOST $RABBITMQ_DEFAULT_USER ".*" ".*" ".*"

